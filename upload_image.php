<?php
	include 'globals.php';
	include ('images/doges/insertNewImage.php');
	session_start(); 

	// This page should only be accessible to users who are signed in.
	if(!$_SESSION['authenticated']) header("Location: main.php");

?>

<!doctype html>

<html>
	<head></head>
	<body background="<?php echo($BACKGROUND); ?>" align="center">
		<?php
			primaryLinks();
			$goodUpload=false;
			$allowedExts = array("gif", "jpeg", "jpg", "png");
			$temp = explode(".", $_FILES["file"]["name"]);
			$extension = end($temp);
			if ((($_FILES["file"]["type"] == "image/gif")
			|| ($_FILES["file"]["type"] == "image/jpeg")
			|| ($_FILES["file"]["type"] == "image/jpg") 
			|| ($_FILES["file"]["type"] == "image/png"))
			&& ($_FILES["file"]["size"] < $MAXUPLOADSIZE)
			&& in_array($extension, $allowedExts)) {
				if($_FILES["file"]["error"] > 0) {
    					echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
  				}else{
    					echo "Upload: " . $_FILES["file"]["name"] . "<br>";
    					echo "Type: " . $_FILES["file"]["type"] . "<br>";
    					echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
    				if(file_exists("images/doges/" . $_FILES["file"]["name"])) {
      					echo $_FILES["file"]["name"] . " already exists. ";
    				}else{
      					move_uploaded_file($_FILES["file"]["tmp_name"],
      					"images/doges/" . $_FILES["file"]["name"]);
    					$goodUpload = true;
					}
  				}
			} else {
		 		echo "Invalid file";
			}
			//NB!!!!!!! have to get user sessions going before we can add author id to the db!.
			// set to -1 as a flag to mark this isnt set up yet!
			addNewImage($_FILES["file"]["name"], $_SESSION['user_id']);
			if($goodUpload){
				echo("<h1>SUCCESS!</h1><br>");
				echo("<h1> Your Image: ".$_FILES["file"]["name"]." has been uploaded! Congratz! :D<br> ");
				$displayImage = sprintf('<img src="images/doges/%s"></img>', $_FILES["file"]["name"]);
				echo($displayImage);
			}
		?>
	</body>
</html>
