<?php
	session_start();
?>
<!doctype html>
<html>

	<head>
		<title>Topdog!</title>
		<?php include "globals.php"; ?>
	</head>
	<body>
	<body background = " <?php echo($BACKGROUND); ?> " style="font-family:comic-sans">


		<div style="color:#ffffff" align="center">
			<?php primaryLinks(); ?>


		<script>
			function validateRegistration(){

				//uname
				if(document.getElementById("uname").value==""){
					document.getElementById("reg_error").innerHTML= "Incomplete detailt:<br> Please enter a username!";
					return false;
				}
				//email
				if(document.getElementById("email").value==""){
					document.getElementById("reg_error").innerHTML="Incomplete detailt:<br> Please enter an email address!!";
					return false;
				}

				//first&last name
				if(document.getElementById("fname").value==""){
					document.getElementById("reg_error").innerHTML="Incomplete detailt:<br> Please enter your first name!!";
					return false;
				}

				if(document.getElementById("lname").value==""){
					document.getElementById("reg_error").innerHTML="Incomplete detailt:<br> Please enter your last name!!";
					return false;
				}

				//passwords
				if(document.getElementById("p1").value !=    document.getElementById("p2").value){
					document.getElementById("reg_error").innerHTML ="Incomplete detailt:<br> The two passwords you entered don't match!";
					return false;
				}

				if(document.getElementById("p1").value==""
				|| document.getElementById("p2").value==""){
					document.getElementById("reg_error").innerHTML = "Incomplete detailt:<br> Please enter a valid password!"
					return false;
				}

				return true;
			}

			function validateLogin(){

				if(document.getElementById("username").value==""){
					document.getElementById("login_error").innerHTML="Error: Please enter a username!";
					return false;
				}

				if( document.getElementById("password").value==""){
					document.getElementById("login_error").innerHTML = "Error: Please enter your password !";
					return false;
				}

				return true;
			}

		</script>

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

		<script>
			//jquery stuff
			$(document).ready(function(){

				$(document.body).hide();
				$(document.body).show(500);

				$("#login_div").hide();
				$("#register_div").hide();


				$("#login_button").click(function(){
					$("#login_div").toggle(500);
				});

				$("#register_button").click(function(){
					$("#register_div").toggle(500);
				});

				

			});

		</script>

		<button id="login_button" style="width:400px;color:green;">Login</button><br>


		<div id="login_div">
			<div id="login_error" style="color:red"></div>
			<form action="login.php" method="post" id="login" onsubmit="return validateLogin()">
				Username:<input type="text" name="username" id="username">
				Password<input type="password" name="password" id="password">
				<input type="submit" value="login!">
			</form><br><br><br>
		</div>


		<button id="register_button" style="width:400px;color:green;">Register</button>


		<div id="register_div">
			<form action="register.php" method="post" id="register" onsubmit="return validateRegistration();">
				<h2>New to topdog? Become a user!!</h2>
				<div style="color:red" id="reg_error">  </div>
				Desired username<input type = "text" name = "username" id="uname"><br>
				Email<input type = "email" name = "email" id="email"><br>
				First name<input type="text" name="firstname" id="fname"><br>
				Last name<input type="text" name="secondname" id="lname"><br>
				Password:<input type = "password" name = "password1" id="p1"><br>
				Retype password:<input type = "password" name = "password2" id="p2"><br>
				<input type = "submit" value = "Register!">
			</form>

		</div>

	</body>

</html>
