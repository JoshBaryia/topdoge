function validate(){

	var validated=true;

	with (document.forms.register)

	if(username.value==""){
		validated=false;
		alert("Please enter a username");
	}

	else if( email.value==""){
		alert("Please enter an email address");
		validated=false;
	}
	else if( firstname.value==""){
		alert("please enter your first name");
		validated=false;
	}
	else if( secondname.value==""){
		alert("Please enter your second name");
		validated=false;
	}

	else if(password1 != password2){
		alert("Your passwords didnt match!");
		validated=false;
	}

	return validated;

}
